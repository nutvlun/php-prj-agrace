<?php
/*
SiXhEaD Pagination - Pagination
Copyright (c) 2005, Chairath Soonthornwiphat (sixhead.com). All rights reserved.
Code licensed under the BSD License: (license.txt)
Version 4.0
*/
class Pagination
{
	var $is_debug		=	FALSE;
	var $db_table		=	"";
	var $order_by		=	"";
	var $primary_key	=	"";
	var $select			=	"*";
	var $custom_sql		=	"";
	var $custom_param	=	"";
	var $per_page		=	10;
	var $page_param		=	"P";
	var $order_param	=	"order";
	var $page_first		=	"&laquo; First";
	var $page_last		=	"Last &raquo;";
	var $page_prev		=	"&#8249; Prev";
	var $page_next		=	"Next &#8250;";
	var $sign_left		=	" ";
	var $sign_right		=	" l";
	var $sign_trim		=	FALSE;
	var $page_now		=	"";
	var $page_of		=	"/";
	var $page_unit		=	"";
	var $page_go		=	"Go";
	var $db_type		=	"MySQL"; # MySQL, MSSQL
	var $style			=	1;


	var $order_uri		=	"";
	var $order_value	=	"";
	var $all_page		=	0;
	var $script_file	=	"";
	var $current_page	=	"";
	var $current_page_prev=	"";
	var $current_page_next=	"";
	var $record_all		=	0;
	var $page_link		=	"";

	function Pagination ()
	{		
	}

	function start()
	{		
		$this->script_file = $_SERVER["PHP_SELF"];
		$this->_do_total_record ();
		$this->_set_page ();
		$this->_set_select_sql ();
	}
	
	function _set_page ()
	{
		$this->current_page = 1;
		if (array_key_exists("$this->page_param",$_REQUEST))
		{ 
			$this->current_page = $_REQUEST["$this->page_param"];
			if (ereg("[^0-9]",$this->current_page) OR $this->current_page == "")
			{ 
				$this->current_page = 1;
			}
		}

		$this->order = "";
		if (array_key_exists("$this->order_param",$_REQUEST))
		{ 
			$this->order = $_REQUEST["$this->order_param"];
		}	

		$this->all_page = ceil ($this->record_all / $this->per_page);
		if ($this->current_page > $this->all_page) { $this->current_page = $this->all_page; }
		
		$this->current_page_prev = $this->current_page - 1;
		$this->current_page_next = $this->current_page + 1;
	}


	function _do_total_record ()
	{
		if ($this->select)
		{ 
			if (preg_match("/,$/",$this->select))
			{ 
				$this->select = preg_replace("/,$/", "", $this->select);
			}
		}
		
		$sql = "";
		if ($this->db_type == "MySQL")
		{ 
			$sql = "SELECT $this->select FROM $this->db_table $this->custom_sql";
			$result = mysql_query($sql);
			$this->record_all = mysql_num_rows($result);
			mysql_free_result($result);				
		}
		else if ($this->db_type == "MSSQL")
		{ 
			if ($this->custom_sql)
			{ 
				$this->custom_sql_mssql_fix = preg_replace("/WHERE/", "AND", $this->custom_sql);
			}
			$sql = "SELECT $this->select FROM $this->db_table $this->custom_sql";
			$result = mssql_query($sql);
			$this->record_all = mssql_num_rows($result);
			mssql_free_result($result);				
		}
		if ($this->is_debug == true) 
		{
			$this->debug ($sql);
		}	
	}

	
	function _set_select_sql ()
	{			
		if ($this->current_page == 1) { $start_loop = 0; }
		else { $start_loop = ($this->current_page - 1) * $this->per_page; }
		if ($start_loop < 0) { $start_loop = 0; }

		if ($this->db_type == "MySQL")
		{ 
			$this->sql = "SELECT $this->select FROM $this->db_table $this->custom_sql ORDER BY $this->order_by LIMIT $start_loop,$this->per_page";
		}
		elseif ($this->db_type == "MSSQL") 
		{ 
			$top_primary_key_before	=	abs ($this->current_page - 1) * $this->per_page;
			$custom_sql_mssql_fix_join = "";
			preg_match("/(left|right) join(.|\n)+?\) /i", $this->custom_sql_mssql_fix, $matches);
			if ($matches[0])
			{ 
				$custom_sql_mssql_fix_join	=	$matches[0];
				$this->custom_sql_mssql_fix		=	preg_replace("/(left|right) join(.|\n)+?\) /i", "", $this->custom_sql_mssql_fix);
			}
			$this->sql = "SELECT TOP $this->per_page $this->select FROM $this->db_table $custom_sql_mssql_fix_join WHERE $this->primary_key NOT IN (SELECT TOP $top_primary_key_before $this->primary_key FROM $this->db_table $this->custom_sql ORDER BY $this->order_by) $this->custom_sql_mssql_fix ORDER BY $this->order_by";
		}


		if ($this->is_debug == true) 
		{
			$this->debug ($this->sql);
		}	
	}


	function get_row ()
	{
		$rows = array();
		if ($this->db_type == "MySQL")
		{
			$result = mysql_query($this->sql);
			if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
			while($row = mysql_fetch_assoc($result)) {
				$rows[] = $row;
			}
			mysql_free_result($result);	
		}
		else if ($this->db_type == "MSSQL")
		{
			$result = mssql_query($this->sql);
			if (!$result) { echo "$sql"; die('Invalid query: ' . mssql_error()); }
			while($row = mssql_fetch_assoc($result)) {
				$rows[] = $row;
			}
			mssql_free_result($result);	
		}
		return $rows;
	}

	function _set_style_1 ()
	{
		$page_links = array();
		for ($i=1;$i<=$this->all_page;$i++)
		{ 
			if ($i == $this->current_page)
			{
				$page_links[] = "$this->sign_left<span class=\"current\">$i</span>$this->sign_right";
			}
			else
			{
				$page_links[] = "$this->sign_left<a href=\"$this->script_file?&amp;$this->page_param=$i$this->custom_param&amp;$this->order_param=$this->order\" class=\"other\">$i</a>$this->sign_right";
			}
		}

		$page_link_all = "";
		$current_page_nav_start = $this->current_page-5;
		$current_page_nav_stop = $this->current_page+3;
		if ($current_page_nav_start > ($this->all_page - 9))
		{
			$current_page_nav_start = $this->all_page - 9;
		}
		if ($current_page_nav_stop < 9)
		{
			$current_page_nav_stop = 9;
		}
		for ($i=$current_page_nav_start;$i<=$current_page_nav_stop;$i++)
		{
			if (array_key_exists("$i",$page_links))
			{ 
				$page_link_all .= $page_links["$i"];						
			}
		}


		$_page_first = "";
		$_page_last = "";
		$_page_prev = "";
		$_page_next = "";
		$sign_left_fix = "";
		$sign_right_fix = "";

		if ($this->sign_trim == TRUE)
		{ 
			if ($this->sign_left == "" || $this->sign_left == " ")
			{
				$sign_left_fix = "&nbsp;";
				$page_link_all	=	preg_replace("/$this->sign_right$/", "&nbsp;", $page_link_all);
			}
		}
		else { 
			if ($sign_left_fix == "") 
			{
				$sign_left_fix = $this->sign_right;
			}
			if ($sign_right_fix == "")
			{
				$sign_right_fix = $this->sign_left;
			}
		}
		


		if ($this->current_page == 1)
		{ 
			$_page_first = "<span>$this->page_first</span> ";
			$_page_prev = "<span>$this->page_prev</span>";
		}
		else
		{ 
			$_page_first = "<a href=\"$this->script_file?&amp;$this->page_param=1$this->custom_param&amp;$this->order_param=$this->order\" class=\"first\">$this->page_first</a> ";
			$_page_prev = "<a href=\"$this->script_file?&amp;$this->page_param=$this->current_page_prev$this->custom_param&amp;$this->order_param=$this->order\" class=\"prev\">$this->page_prev</a>";					
		}

		if ($this->current_page == $this->all_page)
		{ 
			$_page_next = " <span>$this->page_next</span> ";
			$_page_last = " <span>$this->page_last</span>";
		}
		else
		{ 
			$_page_next = " <a href=\"$this->script_file?&amp;$this->page_param=$this->current_page_next$this->custom_param&amp;$this->order_param=$this->order\" class=\"next\">$this->page_next</a> ";
			$_page_last = " <a href=\"$this->script_file?&amp;$this->page_param=$this->all_page$this->custom_param&amp;$this->order_param=$this->order\" class=\"last\">$this->page_last</a>";
		}

		$page_link	=	$_page_first . $_page_prev . $sign_left_fix . $page_link_all . $sign_right_fix . $_page_next . $_page_last;
		$page_link	=	preg_replace("/<a/", "\n<a", $page_link);
		$this->page_link = $page_link;
	}

	function _set_style_2 ()
	{
		$hidden = "";
		foreach (array_keys($_GET) as $key) { 
			if ($key == $this->page_param) { 
				continue;
			}
			$hidden .= "\n<input type=\"hidden\" name=\"$key\" value=\"$_GET[$key]\" />";
		}

		if ($this->current_page == 1)
		{ 
			$_page_prev = "<span>$this->page_prev</span>";
		}
		else
		{ 
			$_page_prev = "<a href=\"$this->script_file?&amp;$this->page_param=$this->current_page_prev$this->custom_param&amp;$this->order_param=$this->order\">$this->page_prev</a>";
		}

		if ($this->current_page == $this->all_page)
		{ 
			$_page_next = "<span>$this->page_next</span>";
		}
		else
		{ 
			$_page_next = "<a href=\"$this->script_file?&amp;$this->page_param=$this->current_page_next$this->custom_param&amp;$this->order_param=$this->order\">$this->page_next</a>";
		}
						
		$this->form_name +=	1;
		$page_link = "<form action=\"$this->script_file\" method=\"get\" id=\"frmP$this->form_name\">\n<div>$_page_prev <span>$this->page_now $this->current_page $this->page_of $this->all_page $this->page_unit</span> $_page_next \n<input name=\"$this->page_param\" type=\"text\" id=\"$this->page_param$this->form_name\" value=\"\" size=\"2\" style=\"text-align:center\"/> <a href=\"javascript:document.getElementById('frmP$this->form_name').submit();\">$this->page_go</a>$hidden</div></form>";
		$this->page_link = $page_link;
	}

	function _get_order_uri ()
	{
		#$this->order_uri	=	$_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"];
		$custom_param_clean =	preg_replace("/&amp;/", "&", $this->custom_param);
		$this->order_uri	=	$_SERVER["REQUEST_URI"];
		$this->order_uri	=	preg_replace("/(&|)$this->page_param=(.*?)&/", "&", $this->order_uri);
		$this->order_uri	=	preg_replace("/(&|)$this->page_param=(.*?)$/", "", $this->order_uri);
		$this->order_uri	=	preg_replace("/$custom_param_clean/", "", $this->order_uri);

		if (!ereg("\?",$this->order_uri)) { $this->order_uri .= "?"; }
		$this->order_value = "";
		if (preg_match("/&$this->order_param=(.*?)&/",$this->order_uri, $matches))
		{
			$this->order_value = urldecode($matches[1]);
			$this->order_uri = preg_replace("/&$this->order_param=(.*?)&/", "&", $this->order_uri);	
		}
		else if (preg_match("/&$this->order_param=(.*?)$/",$this->order_uri, $matches))
		{ 
			$this->order_value = urldecode($matches[1]);
			$this->order_uri = preg_replace("/&$this->order_param=(.*?)$/", "", $this->order_uri);	
		}

		$this->order_uri = preg_replace("/&/", "&amp;", $this->order_uri);
	}

	function get_sort_link($text,$key_ASC,$key_DESC,$default_sort="DESC")
	{		
		$default_sort = strtoupper($default_sort);
		$image_link	= "";
		$new_order = "";
		$sort_link = "";
		if ($this->order_value == $key_ASC)
		{
			$new_order = $key_DESC;
			$image_link = "<img src=\"" . $this->sort_icon["ASC"] . "\" style=\"boder:0\" alt=\"ASC\"/>";
		}
		else if ($this->order_value == $key_DESC)
		{ 
			$new_order = $key_ASC;
			$image_link = "<img src=\"" . $this->sort_icon["DESC"] . "\" style=\"boder:0\" alt=\"DESC\"/>";
		}
		else 
		{ 
			$new_order = ${"key_$default_sort"};
			$image_link = "";
		}

		$sort_link = "<a href=\"$this->order_uri&amp;$this->order_param=$new_order$this->custom_param\">$text</a>$image_link";
		return $sort_link;
	}
	
	function set_sort_icon ($sort_icon_asc,$sort_icon_desc)
	{
		$this->sort_icon["ASC"]		=	$sort_icon_asc;
		$this->sort_icon["DESC"]	=	$sort_icon_desc;
		$this->_get_order_uri();
	}


	function get_page_link ()
	{
		if ($this->style == 1) { $this->_set_style_1(); }
		else if ($this->style == 2) { $this->_set_style_2(); }
		if ($this->record_all <= $this->per_page) { $this->page_link = ""; }

		return $this->page_link;
	}


	function get_record_all ()
	{
		return $this->record_all;
	}


	function get_sql ()
	{
		return $this->sql;
	}
	function get_all_page(){
		return	$this->all_page;
	}
	function get_prev_page(){
		return	$this->current_page_prev;
	}
	function get_next_page(){
		return	$this->current_page_next;
	}
	

	function debug($str)
	{
		echo "<pre>";
		print_r($str);
		echo "</pre>";
	}
}

?>