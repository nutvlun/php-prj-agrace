<?
/*------------------------------------------------------------------*/
/*- Require --------------------------------------------------------*/

include("module/_config.php");
include("module/SiXhEaD.Template.php");
include("module/SiXhEaD.Pagination.php");
include("module/_module.php");



/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/

$tp_index		=	"_tp_index.html";

$strTopMenu		=	getTopMenu();
$strFooter		=	getFooter();
/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");

/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/


/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

$tp = new Template($tp_index);
$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");

/*---------------- Testimonials --------------------------*/
$sql	=	"SELECT `NewsId` As TestimonialId,`Title` As TestimonialTitle,`ShortDetail` As TestimonialShortDetail,`File1` As TestimonialFile1 FROM $strCfgDbTableNews WHERE  (`Active`='Y' AND `IsDelete`='N') AND `Cate` ='testimonials' AND ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC LIMIT 0,1";
$result	=	mysql_query($sql);
@extract(mysql_fetch_assoc($result));
$ImgTestimonials	=	"module/phpThumb/phpThumb.php?src=../../content/news/$TestimonialFile1&w=180&h=140&zc=1&sx=0";
/*---------------- // Testimonials --------------------------*/
/*---------------- News & Promotion --------------------------*/
$sql	=	"SELECT `NewsId` ,`Title` As NewsTitle,`ShortDetail` As NewsShortDetail,`File1` As NewsFile1 FROM $strCfgDbTableNews WHERE  (`Active`='Y' AND `IsDelete`='N') AND `Cate` ='news' AND ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC LIMIT 0,4";
$result	=	mysql_query($sql);
$tp->block("DataNews");
$k=0;
$i=0;
$divOpen="<div>";
while($row = mysql_fetch_array($result)) {
	$Id					=	$row["NewsId"];
	$TblID = <<<Data
ID="$NewsId";
Data;
	$NewsTitle				=	$row["NewsTitle"];
	$NewsShortDetail			=	$row["NewsShortDetail"];
	$NewsFile1					=	$row["NewsFile1"];
	
	$ImgNews = <<<img
module/phpThumb/phpThumb.php?src=../../content/news/$NewsFile1&w=280&h=180&zc=1&sx=0
img;
	if($k==0)$divOpen="<div>";
	$k++;
	if($k>1){
		$divClose="</div>";
		$k=0;
	}
	if($i==($intRecordAll-1)){
		$divClose="</div>";
	}
	$tp->apply();
	$divOpen="";
	$divClose="";
	$i++;
}

/*----------------// News & Promotion --------------------------*/

/*---------------- A Grace Program --------------------------*/
$sql	=	"SELECT `NewsId` As AGraceId ,`Title` As AGraceTitle,`ShortDetail` As AGraceDetail,`File1` As AGraceFile1 FROM $strCfgDbTableNews WHERE  (`Active`='Y' AND `IsDelete`='N') AND `Cate` ='our_service' AND ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC";
$result	=	mysql_query($sql);
$tp->block("DataAGrace");

$i=0;

while($row = mysql_fetch_array($result)) {
	$AGraceId					=	$row["AGraceId"];
	$TblID = <<<Data
ID="$AGraceId";
Data;

	
	$AGraceTitle				=	$row["AGraceTitle"];
	$AGraceShortDetail			=	$row["AGraceShortDetail"];
	$AGraceFile1					=	$row["AGraceFile1"];
	
	$ImgAGrace = <<<img
module/phpThumb/phpThumb.php?src=../../content/news/$AGraceFile1&w=202&h=120&zc=1&sx=0
img;
	$tp->apply();
	$i++;
}

/*----------------// A Grace Program --------------------------*/
/*---------------- AG By A Grace --------------------------*/
$sql	=	"SELECT `NewsId` As AgId,`Title` As AgTitle,`ShortDetail` As AgShortDetail,`File1` As AgFile1 FROM $strCfgDbTableNews WHERE  (`Active`='Y' AND `IsDelete`='N') AND `Cate` ='ag_by_agrace' AND ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC LIMIT 0,1";
$result	=	mysql_query($sql);
@extract(mysql_fetch_assoc($result));
$ImgAg	=	"module/phpThumb/phpThumb.php?src=../../content/news/$AgFile1&w=250&h=250&zc=1&sx=0";
/*---------------- // AG By A Grace --------------------------*/
/*---------------- Doctor --------------------------*/
$sql	=	"SELECT `NewsId` As DoctorId,`Title` As DoctorTitle,`ShortDetail` As DoctorShortDetail,`File1` As DoctorFile1 FROM $strCfgDbTableNews WHERE  (`Active`='Y' AND `IsDelete`='N') AND `Cate` ='doctor' AND ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC LIMIT 0,1";
$result	=	mysql_query($sql);
@extract(mysql_fetch_assoc($result));
$ImgDoctor	=	"module/phpThumb/phpThumb.php?src=../../content/news/$DoctorFile1&w=310&h=140&zc=1&sx=0";
/*---------------- // Doctor --------------------------*/

/*---------------- Beauty Talk --------------------------*/
$sql	=	"SELECT `NewsId` As BeautyTalkId,`Title` As BeautyTalkTitle,`ShortDetail` As BeautyTalkShortDetail,`File1` As BeautyTalkFile1 FROM $strCfgDbTableNews WHERE  (`Active`='Y' AND `IsDelete`='N') AND `Cate` ='beauty_talk' AND ActivityDate<='$Today' ORDER BY  Mark DESC,LineNo ASC,`NewsId` DESC LIMIT 0,1";
$result	=	mysql_query($sql);
@extract(mysql_fetch_assoc($result));
$ImgBeautyTalk	=	"module/phpThumb/phpThumb.php?src=../../content/news/$TestimonialFile1&w=250&h=250&zc=1&sx=0";
/*---------------- // Beauty Talk --------------------------*/
mysql_free_result($result);
mysql_close($conn);
$tp->Display();
exit;


/*------------------------------------------------------------------*/
?>