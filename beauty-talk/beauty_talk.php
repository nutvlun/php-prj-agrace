<?
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*------------------------------------------------------------------*/
/*

Program     : 
Description : 
Programmer  : ตรินัยน์ จันทร์คง

Individual
Email       : thrinai@hotmail.com
Website     : 


Office
Email       : thrinai@digithais.com
Website     : http://www.digithais.com

Date        : 02-07-2009
Modify log  : 

*/
/*------------------------------------------------------------------*/
/*- Include Library --------------------------------------------------------*/
include("../module/_config.php");
include("../module/_module.php");
include("../module/SiXhEaD.Template.php");
include("../module/SiXhEaD.Pagination.php");
/*------------------------------------------------------------------*/
/*- Template -------------------------------------------------------*/


$tp_index			=	"_tp_index.html";

$strTopMenu		=	getTopMenu();
$strFooter		=	getFooter();
/*------------------------------------------------------------------*/
/*- Config & Misc --------------------------------------------------*/

$Today	 =	 date("Y-m-d");


/*------------------------------------------------------------------*/
/*- Request --------------------------------------------------------*/

$cate				=	$_REQUEST[cate];
$searchq		=	$_REQUEST[searchq];
$order_by	=	$_REQUEST[order_by];
$order			=	$_REQUEST[order];
$LimitPage	=	$_REQUEST[LimitPage];
$page			=	($_REQUEST[page]=="")?1:$_REQUEST[page];

/*------------------------------------------------------------------*/
/*- Program --------------------------------------------------------*/

$tp = new Template($tp_index);

$conn=mysql_connect ($strCfgDbHost,$strCfgDbUser,$strCfgDbPass);
if (!$conn) {die('Not connected : ' . mysql_error());}
// make foo the current db
$db_selected = mysql_select_db($strCfgDbName, $conn);
if (!$db_selected) { die ('Can\'t use $strCfgDbName : ' . mysql_error());}
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
//- จัด Range ใหม่

if(!$LimitPage){$LimitPage=24;}
//- Condition

$WHERE = " ";
$order_by = " Mark DESC,LineNo ASC,`NewsId` DESC ";
$WHERE	=	" AND `Cate`='beauty_talk' AND  ActivityDate<='$Today'";


$sp	= New Pagination();
$sp->db_type			=	"MySQL";
$sp->db_table			=	$strCfgDbTableNews;
$sp->order_by			=	$order_by; 
$sp->primary_key	=	"`NewsId`";
$sp->select			=	"`NewsId`,`LineNo`,`Mark`,`Title`,`ShortDetail`,`File1`,ActivityDate";
$sp->custom_sql		=	" WHERE  IsDelete='N' And `Active`='Y' $WHERE  " ; # WHERE, JOIN
$sp->custom_param	=	"&LimitPage=$LimitPage&cate=$cate&searchq=$searchq";
$sp->per_page		=	$LimitPage;
$sp->order_param	=	"order"; # $_GET["order"];
$sp->page_param	=	"page"; # $_REQUEST["p"];
$sp->page_prev		=	"&#8249; Prev"; //<span class="yellow12" style="font-weight: bold">1</span> | <a href="#" class="white12-a">2</a> | 3 | 4 | 5
$sp->page_next		=	"Next &#8250;";
$sp->page_first		=	"&laquo; First";
$sp->page_last		=	"Last &raquo;";
$sp->sign_left			=	"";
$sp->sign_right		=	" |";
$sp->sign_trim		=	FALSE;
$sp->style				=	1;
$sp->start();

$intRecordAll	=	$sp->get_record_all();
$sql				=	$sp->get_sql();
$strPageLink	=	$sp->get_page_link();
$intPageAll		=	$sp->get_all_page();

$bg_color = "";
$result = mysql_query($sql); //echo $sql;
if (!$result) { echo "$sql"; die('Invalid query: ' . mysql_error()); }
mysql_query("SET NAMES UTF8");
mysql_query("SET character_set_results=UTF8");
$strTbl	=	"";
$tp->block("DATA");
$k=0;
$i=0;
$divOpen="<div>";
while($row = mysql_fetch_array($result)) {
	$Id					=	$row["NewsId"];
	$TblID = <<<Data
ID="$NewsId";
Data;
	$LineNo				=	$row["LineNo"];
	$Mark					=	$row["Mark"];
	$Title					=	$row["Title"];
	$ShortDetail			=	$row["ShortDetail"];
	$File1					=	$row["File1"];
	
	$Img = <<<img
../module/phpThumb/phpThumb.php?src=../../content/news/$File1&w=280&h=180&zc=1&sx=0
img;
	if($k==0)$divOpen="<div>";
	$k++;
	if($k>5){
		$divClose="</div>";
		$k=0;
	}
	if($i==($intRecordAll-1)){
		$divClose="</div>";
	}
	$tp->apply();
	$divOpen="";
	$divClose="";
	$i++;
}
mysql_free_result($result);
mysql_close($conn);



$tp->Display();
exit;

?>