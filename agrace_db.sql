-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 20, 2016 at 01:12 AM
-- Server version: 5.1.30
-- PHP Version: 5.2.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agrace_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `Id` int(2) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL DEFAULT '',
  `Password` varchar(30) NOT NULL DEFAULT '',
  `Fname` varchar(100) DEFAULT NULL,
  `Lname` varchar(100) DEFAULT NULL,
  `AdminType` enum('SuperAdmin','Admin') NOT NULL DEFAULT 'Admin',
  `Email` varchar(100) NOT NULL DEFAULT '',
  `AddDate` datetime DEFAULT NULL,
  `Active` enum('N','Y') NOT NULL DEFAULT 'Y',
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Id`, `Username`, `Password`, `Fname`, `Lname`, `AdminType`, `Email`, `AddDate`, `Active`, `UpdateDate`) VALUES
(1, 'admin', 'password', 'Administrator', NULL, 'SuperAdmin', '', '2011-05-24 12:31:24', 'Y', '2016-03-07 11:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
  `IsDelete` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  `LineNo` int(11) DEFAULT '999999999',
  `Mark` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `Cate` enum('news','testimonials','beauty_talk','ag_by_agrace','our_service','doctor') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'news',
  `Title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ActivityDate` date DEFAULT NULL,
  `ShortDetail` text COLLATE utf8_unicode_ci,
  `Detail` longtext COLLATE utf8_unicode_ci,
  `File1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  `AddDate` datetime DEFAULT NULL,
  `UpdateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NewsId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=94 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`NewsId`, `IsDelete`, `LineNo`, `Mark`, `Cate`, `Title`, `ActivityDate`, `ShortDetail`, `Detail`, `File1`, `Active`, `AddDate`, `UpdateDate`) VALUES
(1, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'ราคา xxx -xxx บาท', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(2, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(3, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(4, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(5, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(6, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(7, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(8, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(9, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(10, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(11, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(12, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(13, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(14, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(15, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(16, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(17, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(18, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(19, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(20, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(21, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(22, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(23, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(24, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(25, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(26, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(27, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(28, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(29, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(30, 'N', 999999999, 'N', 'news', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(31, 'N', 999999999, 'N', 'ag_by_agrace', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(32, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(33, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(34, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(35, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(36, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(37, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(38, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(39, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(40, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(41, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(42, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(43, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(44, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(45, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(46, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(47, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(48, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(49, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(50, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(51, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(52, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(53, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(54, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(55, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(56, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(57, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(58, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(59, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(60, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(61, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(62, 'N', 999999999, 'N', 'testimonials', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(63, 'N', 999999999, 'N', 'testimonials', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(64, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(65, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(66, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(67, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(68, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(69, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(70, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(71, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(72, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(73, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(74, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(75, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(76, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(77, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(78, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(79, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(80, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(81, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(82, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(83, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(84, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(85, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(86, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(87, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(88, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(89, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(90, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(91, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37'),
(92, 'N', 999999999, 'N', 'beauty_talk', 'test_news1', '2016-03-07', 'test_news1', '<p>test_news1</p>', '1-File1.jpg', 'Y', '2016-03-07 15:21:08', '2016-03-07 15:21:08'),
(93, 'N', 999999999, 'N', 'beauty_talk', 'test_testimonials1', '2016-03-07', 'test_testimonials1', '<p>test_testimonials1</p>', '2-File1.jpg', 'Y', '2016-03-07 15:23:37', '2016-03-07 15:23:37');
